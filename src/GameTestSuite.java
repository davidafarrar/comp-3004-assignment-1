import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	engine.FighterEngineTests.class,
	engine.GameEngineTests.class,
})

public class GameTestSuite {   
} 
