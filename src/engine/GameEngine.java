package engine;

public class GameEngine {

	private int rounds = 0;
	private int numPlayers = 0;
	private int attackCounter = 0;
	private int roundCounter = 0;
	private FighterEngine[] players;
	
	public int gameState = SETUP;
	
	public static final int SETUP = 0;
	public static final int WAITING = 1;
	public static final int STARTED = 2;
	public static final int ROUNDOVER = 3;
	public static final int GAMEOVER = 4;
	
	public GameEngine(){
		this.rounds = 0;
		this.numPlayers = 0;
	}
	
	public String start(int rounds,int numPlayers,String[] names){
		this.rounds = rounds;
		this.numPlayers = numPlayers;
		players = new FighterEngine[numPlayers];
		
		String output = "";
		for(int i = 0; i<numPlayers; i++){
			players[i]= new FighterEngine(names[i]);
			output += names[i] + ", ";
		}
		gameState = STARTED;
		return "Game Started with " + output;
	}
	
	public String attack(String from, String to, String move, int speed, int roll){
		for(FighterEngine e : players){
			if(e.name==to){
				e.processInput(move, speed, roll);
				attackCounter++;
				if(roundCounter == numPlayers){
					System.out.println(getRoundSum());
				}
				return "Confirm";
			}
		}
		return "Non Existant Target";
	}
	
	public String defend(String from, String move, int speed){
		for(FighterEngine e : players){
			if(e.name==from){
				e.processInput(move, speed, 0);
				return "Confirm";
			}
		}
		return "Non Existant Target";
	}
	
	public String getRoundSum(){	
		String output = "";
		for(FighterEngine e : players){
			output += e.name + "-" + e.getNumWounds() + "hits, ";
		}
		roundCounter++;
		attackCounter = 0;
		
		return output;
	}
	
	
	public int getNumPlayers(){
		return numPlayers;
	}
	
}
