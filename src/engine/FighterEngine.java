package engine;

public class FighterEngine {
	
	public static final int HALT = 0; 
	public static final int DEFENSE = 1;
	public static final int ATTACK = 2;
	
	private int state = HALT;
	
	private String defenseMove;
	private int defenseSpeed;
	
	private int numWounds;
	public String name;
	
	//private String[] attacks = {"Thrust","Swing","Smash"};
	//private String[] defenses = {"Charge","Dodge","Duck"};
	
	
	public FighterEngine (String name) {
		defenseMove = "";
		defenseSpeed = 0;
		numWounds = 0;
		this.name = name;
	}
	
	public String processInput (String move, int speed, int roll){
		if(move!=null && speed>0){
			if(state==DEFENSE){
				defenseMove = move;
				defenseSpeed = speed;
				state = ATTACK;
				return "Confirm";
			}
			else if(state==ATTACK){
				return processAttack(processRoll(move,roll),speed);
			}
			return "Error: Incorrect Input";
		}
		else
			return "Error: Incorrect Setup";
	}
	
	public String processRoll (String attackMove,int roll){
		if(roll==0)
			System.out.println("Error invalid roll");
		if(attackMove == "Thrust"){
			if(roll==1 || roll==2){
				return "Thrust";
			}
			else if(roll==3 || roll ==4){
				return "Smash";
			}
			else if(roll==5 || roll ==6){
				return "Swing";
			}
			else
				return "Error";
		}
		else if(attackMove == "Swing"){
			if(roll==1 || roll==2){
				return "Swing";
			}
			else if(roll==3 || roll ==4){
				return  "Thrust";
			}
			else if(roll==5 || roll ==6){
				return  "Smash";
			}
			else
				return "Error";
		}
		else if(attackMove == "Smash"){
			if(roll==1 || roll==2){
				return  "Smash";
			}
			else if(roll==3 || roll ==4){
				return  "Swing";
			}
			else if(roll==5 || roll ==6){
				return "Thrust";
			}
			else
				return "Error";
		}
		else{
			return "Error: Incorrect Input";
		}


		
	}
	
	private String processAttack (String attackMove,int attackSpeed){
		if(attackMove=="Thrust" && defenseMove=="Charge"){
			numWounds++;
			return "Confirm";
		}
		else if(attackMove=="Swing" && defenseMove=="Dodge"){
			numWounds++;
			return "Confirm";
		}
		else if(attackMove=="Smash" && defenseMove=="Duck"){
			numWounds++;
			return "Confirm";
		}
		else if(attackSpeed<defenseSpeed){
			numWounds++;
			return "Confirm";
		}
		else{
			return "Denied";
		}	
	}
	
	public void setState (int state){
		this.state=state;
	}
	
	public int getNumWounds (){
		return numWounds;
	}
	
	public void reset(){
		defenseMove = "";
		defenseSpeed = 0;
		numWounds = 0;
		state=DEFENSE;
	}
}
