package engine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import engine.FighterEngine;

public class FighterEngineTests {
	FighterEngine fEngine;

	/** This will be processed before the Test Class is instantiated */
    @BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass");
    }
    
    /** This is processed/initialized before each test is conducted */
    @Before
    public void setUp() {
		System.out.println("@Before()");
		fEngine = new FighterEngine("Default");
	}
	
    /** This is processed/initialized after each test is conducted */
    @After
    public void tearDown () {
		System.out.println("@After()");
		fEngine = null;
	}
	
    /** This will be processed after the Test Class has been destroyed */
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass");
    }
    
    @Test
	public void Test01() {
		System.out.println("@Test(): Rule01");
		fEngine.setState(FighterEngine.DEFENSE);
		
		/** Tests for 3-1*/
		assertEquals("Confirm",fEngine.processInput("Dodge", 3,0));
		assertEquals("Confirm",fEngine.processInput("Thrust",1,1));
		assertEquals(1, fEngine.getNumWounds());
		fEngine.reset();
		
		/** Tests for 2-2*/
		assertEquals("Confirm",fEngine.processInput("Dodge", 2,0));
		assertEquals("Denied",fEngine.processInput("Thrust",2,1));
		assertEquals(0, fEngine.getNumWounds());
		fEngine.reset();
		
		/** Tests for 1-3*/
		assertEquals("Confirm",fEngine.processInput("Dodge", 1,0));
		assertEquals("Denied",fEngine.processInput("Thrust",3,1));
		assertEquals(0, fEngine.getNumWounds());
		fEngine.reset();
	}

    @Test
	public void Test02() {
		System.out.println("@Test(): Rule02");
		fEngine.setState(FighterEngine.DEFENSE);
		
		/** Tests for Process Roll function 1*/
		assertEquals("Thrust",fEngine.processRoll("Thrust",1));
		
		assertEquals("Smash",fEngine.processRoll("Thrust",3));
		
		assertEquals("Swing",fEngine.processRoll("Thrust",5));
		
		assertEquals("Smash",fEngine.processRoll("Smash",1));
		
		assertEquals("Swing",fEngine.processRoll("Smash",3));
		
		assertEquals("Thrust",fEngine.processRoll("Smash",5));
		
		assertEquals("Swing",fEngine.processRoll("Swing",1));

		assertEquals("Thrust",fEngine.processRoll("Swing",3));

		assertEquals("Smash",fEngine.processRoll("Swing",5));

	}
    
    
    @Test
	public void Test03() {
		System.out.println("@Test(): Rule02");
		fEngine.setState(FighterEngine.DEFENSE);
		
		/** Tests for Thrust*/
		assertEquals("Confirm",fEngine.processInput("Charge",1,0));
		assertEquals("Confirm",fEngine.processInput("Thrust",1,1));
		assertEquals(1, fEngine.getNumWounds());
		fEngine.reset();
		
		assertEquals("Confirm",fEngine.processInput("Duck", 1,0));
		assertEquals("Denied",fEngine.processInput("Thrust",1,1));
		assertEquals(0, fEngine.getNumWounds());
		fEngine.reset();
		
		assertEquals("Confirm",fEngine.processInput("Dodge", 1,0));
		assertEquals("Denied",fEngine.processInput("Thrust",1,1));
		assertEquals(0, fEngine.getNumWounds());
		fEngine.reset();
		
		/** Tests for Smash*/
		assertEquals("Confirm",fEngine.processInput("Duck", 1,0));
		assertEquals("Confirm",fEngine.processInput("Smash",1,1));
		assertEquals(1, fEngine.getNumWounds());
		fEngine.reset();
		
		assertEquals("Confirm",fEngine.processInput("Dodge", 1,0));
		assertEquals("Denied",fEngine.processInput("Smash",1,1));
		assertEquals(0, fEngine.getNumWounds());
		fEngine.reset();
		assertEquals("Confirm",fEngine.processInput("Charge", 1,0));
		assertEquals("Denied",fEngine.processInput("Smash",1,1));
		assertEquals(0, fEngine.getNumWounds());
		fEngine.reset();
		
		/** Tests for Swing*/
		assertEquals("Confirm",fEngine.processInput("Dodge", 1,0));
		assertEquals("Confirm",fEngine.processInput("Swing",1,1));
		assertEquals(1, fEngine.getNumWounds());
		fEngine.reset();
		
		assertEquals("Confirm",fEngine.processInput("Charge", 1,0));
		assertEquals("Denied",fEngine.processInput("Swing",1,1));
		assertEquals(0, fEngine.getNumWounds());
		fEngine.reset();
		
		assertEquals("Confirm",fEngine.processInput("Duck", 1,0));
		assertEquals("Denied",fEngine.processInput("Swing",1,1));
		assertEquals(0, fEngine.getNumWounds());
		fEngine.reset();
	}
    
    @Test
	public void Test04() {
		System.out.println("@Test(): Rule04");
		fEngine.setState(FighterEngine.DEFENSE);
		
		assertEquals("Confirm",fEngine.processInput("Duck", 3,0));
		assertEquals("Confirm",fEngine.processInput("Smash",1,1));
		assertEquals(1, fEngine.getNumWounds());
		fEngine.reset();
	}
    
    @Test
	public void Test05() {
		System.out.println("@Test(): Rule05");
		fEngine.setState(FighterEngine.DEFENSE);
		
		assertEquals("Confirm",fEngine.processInput("Duck", 2,0));
		assertEquals("Denied",fEngine.processInput("Swing",2,1));
		assertEquals(0, fEngine.getNumWounds());
		fEngine.reset();
	}

}
