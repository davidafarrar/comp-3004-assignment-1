package engine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import engine.FighterEngine;

public class GameEngineTests {
	GameEngine gEngine;

	/** This will be processed before the Test Class is instantiated */
    @BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass");
    }
    
    /** This is processed/initialized before each test is conducted */
    @Before
    public void setUp() {
		System.out.println("@Before()");
		gEngine = new GameEngine();
	}
	
    /** This is processed/initialized after each test is conducted */
    @After
    public void tearDown () {
		System.out.println("@After()");
		gEngine = null;
	}
	
    /** This will be processed after the Test Class has been destroyed */
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass");
    }
    
    @Test
	public void Test01() {
		System.out.println("@Test(): Test01");
		String[] temp = {"joe","bob"};
		gEngine.start(1,2,temp);
		
		assertEquals("Confirm",gEngine.defend("joe","Dodge",3));
		assertEquals("Confirm",gEngine.defend("bob", "Duck", 3));
		assertEquals("Confirm",gEngine.attack("bob","joe","Thrust",1,1));
		assertEquals("Confirm",gEngine.attack("joe","bob","Thrust",1,1));
		assertEquals("joe-0hits, bob-0hits, ",gEngine.getRoundSum());
	}
    
    @Test
	public void Test02() {
		System.out.println("@Test(): Test02");
		String[] temp = {"jack","joe","fred"};
		gEngine.start(3,3,temp);
		
		assertEquals("Confirm",gEngine.defend("joe","Duck",3));
		assertEquals("Confirm",gEngine.defend("fred", "Dodge", 2));
		assertEquals("Confirm",gEngine.defend("jack", "Duck", 2));
		assertEquals("Confirm",gEngine.attack("fred","joe","Swing",1,1));
		assertEquals("Confirm",gEngine.attack("joe","fred","Swing",2,1));
		assertEquals("Confirm",gEngine.attack("joe","jack","Smash",1,1));
		assertEquals("jack-0hits, joe-0hits, fred-0hits, ",gEngine.getRoundSum());
	}

}
